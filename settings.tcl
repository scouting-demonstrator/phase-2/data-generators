if { [ info exists env(CLOCK) ] } { set clock $env(CLOCK); } { set clock "2.6";  }
if { [ info exists env(CLOCK_UNCERTAINTY) ] } { set clockUncertainty $env(CLOCK_UNCERTAINTY); } { set clockUncertainty "auto" } 
if { [ info exists env(FPGA_PART) ] } { set fpgaPart $env(FPGA_PART); } { set fpgaPart "xcku15p-ffva1760-2-e" }

proc make_solution { fpgaPart clock clockUncertainty } { 
    open_solution -reset "solution"
    set_part $fpgaPart 
    create_clock -period $clock 
    if { $clockUncertainty != "auto" } {
        set_clock_uncertainty $clockUncertainty
    }
}